const appRoot = document.getElementById('app-root');
const table = document.createElement('table');
const thead = document.createElement('thead');
const headerRow = document.createElement('tr');
const headers = [
  'Название страны',
  'Столица',
  'Регион',
  'Языки',
  'Площадь',
  'Флаг',
];

//Сортировка в алфавитном порядке и по возрастанию
function sortByKey(array, key, order = 'asc') {
  return array.sort((a, b) => {
    const valA = typeof a[key] === 'string' ? a[key].toUpperCase() : a[key];
    const valB = typeof b[key] === 'string' ? b[key].toUpperCase() : b[key];
    if (order === 'asc') {
      return valA > valB ? 1 : valA < valB ? -1 : 0;
    } else {
      return valA < valB ? 1 : valA > valB ? -1 : 0;
    }
  });
}

const sortOrder = {
  name: 'no sort',
  area: 'no sort',
};

function checkSortedMode() {
  if (mode === 'По региону') {
    if (region === 'All' || '') {
      return externalService.getAllCountries();
    } else {
      return externalService.getCountryListByRegion(region);
    }
  } else if (mode === 'По языку') {
    if (language === 'All' || '') {
      return externalService.getAllCountries();
    } else {
      return externalService.getCountryListByLanguage(language);
    }
  } else {
    return externalService.getAllCountries();
  }
}
function createSortButton(onClick) {
  const button = document.createElement('button');
  button.classList.add('button');
  button.addEventListener('click', onClick);
  return button;
}
function sortByName(order) {
  const sortedCountries = sortByKey(checkSortedMode(), 'name', order);
  tbodyRender(sortedCountries);
}

// Функция сортировки по площади
function sortByArea(order) {
  const sortedCountries = sortByKey(checkSortedMode(), 'area', order);
  tbodyRender(sortedCountries);
}

const nameSortButton = createSortButton(() => {
  sortOrder.name = sortOrder.name === 'asc' ? 'desc' : 'asc';
  sortByName(sortOrder.name);
});

const areaSortButton = createSortButton(() => {
  sortOrder.area = sortOrder.area === 'asc' ? 'desc' : 'asc';
  sortByArea(sortOrder.area);
});

//Сама таблица
headers.forEach((headerText) => {
  const th = document.createElement('th');
  th.textContent = headerText;
  th.classList.add('header');
  headerRow.appendChild(th);
  if (th.textContent === 'Название страны') {
    th.appendChild(nameSortButton);
  }
  if (th.textContent === 'Площадь') {
    th.appendChild(areaSortButton);
  }
});
thead.appendChild(headerRow);
table.appendChild(thead);

const tbody = document.createElement('tbody');

function tbodyRender(array) {
  tbody.innerHTML = '';
  array.forEach((country) => {
    const row = document.createElement('tr');
    row.classList.add('row-body');
    //Название страны
    const nameCell = document.createElement('td');
    nameCell.textContent = country.name;
    nameCell.classList.add('text');
    row.appendChild(nameCell);

    //Столица
    const capitalCell = document.createElement('td');
    capitalCell.textContent = country.capital;
    capitalCell.classList.add('text');
    row.appendChild(capitalCell);

    //Регион
    const regionCell = document.createElement('td');
    regionCell.textContent = country.region;
    regionCell.classList.add('text');
    row.appendChild(regionCell);

    //Язык
    const languagesCell = document.createElement('td');
    languagesCell.textContent = Object.values(country.languages).join(', ');
    languagesCell.classList.add('text');
    row.appendChild(languagesCell);

    //Площадь
    const areaCell = document.createElement('td');
    areaCell.textContent = country.area;
    areaCell.classList.add('text');
    row.appendChild(areaCell);

    //Флаг
    const flagCell = document.createElement('td');
    const flagImage = document.createElement('img');
    flagImage.classList.add('flag');
    flagImage.src = country.flagURL;
    flagImage.alt = `${country.name} Flag`;
    flagCell.appendChild(flagImage);
    row.appendChild(flagCell);

    //Вставить в тело таблицы
    tbody.appendChild(row);
  });
}

//Конец таблицы

const searchContainer = document.createElement('div');
searchContainer.classList.add('search-container');
appRoot.appendChild(searchContainer);
const searchBy = ['Без фильтрации', 'По региону', 'По языку'];
let mode = '';

//Выбор типа фильтрации
function createRadioInput(name, value, id, labelText, onChangeCallback) {
  const inputBox = document.createElement('div');
  inputBox.classList.add('input-box');

  const radioInput = document.createElement('input');
  radioInput.type = 'radio';
  radioInput.name = name;
  radioInput.value = value;
  radioInput.id = id;

  const label = document.createElement('label');
  label.textContent = labelText;
  label.htmlFor = id;

  radioInput.addEventListener('change', onChangeCallback);

  inputBox.appendChild(radioInput);
  inputBox.appendChild(label);

  return inputBox;
}

//Фунция для фильтрации по региону
function handleRegionChange() {
  region = regionSelect.value;
  if (region === 'All') {
    tbodyRender(externalService.getAllCountries());
  } else {
    tbodyRender(externalService.getCountryListByRegion(region));
  }
}

//Фунция для фильтрации по языку
function handleLanguageChange() {
  language = languageSelect.value;
  if (language === 'All') {
    tbodyRender(externalService.getAllCountries());
  } else {
    tbodyRender(externalService.getCountryListByLanguage(language));
  }
}

// Создание радио кнопки для переключения фильтрации
searchBy.forEach((modeText) => {
  const radioInput = createRadioInput(
    'searchMode',
    modeText,
    modeText,
    modeText,
    function () {
      mode = modeText;
      if (mode === 'По региону') {
        tbodyRender(externalService.getAllCountries());
        searchContainer.appendChild(regionSelect);
        regionSelect.addEventListener('change', handleRegionChange);
        removeLanguage();
      } else if (mode === 'По языку') {
        tbodyRender(externalService.getAllCountries());
        searchContainer.appendChild(languageSelect);
        languageSelect.addEventListener('change', handleLanguageChange);
        removeRegion();
      } else if (mode === 'Без фильтрации') {
        tbodyRender(externalService.getAllCountries());
        removeLanguage();
        removeRegion();
      }
    }
  );

  searchContainer.appendChild(radioInput);
});

//Селект регионов
const regionSelect = document.createElement('select');
const regions = externalService.getRegionsList();
regions.unshift('All');
let region = '';

regions.forEach((regionText) => {
  const optionRegion = document.createElement('option');
  optionRegion.textContent = regionText;
  regionSelect.appendChild(optionRegion);
});

//Очистка для переключения с региона на язык
function removeRegion() {
  regionSelect.value = 'All';
  languageSelect.value = 'All';
  region = 'All';
  regionSelect.remove();
}

//Селект языков
const languageSelect = document.createElement('select');
const languages = externalService.getLanguagesList();
languages.unshift('All');
let language = '';
languages.forEach((languageText) => {
  const optionLanguage = document.createElement('option');
  optionLanguage.textContent = languageText;
  languageSelect.appendChild(optionLanguage);
});

//Очистка для переключения с языка на регион
function removeLanguage() {
  regionSelect.value = 'All';
  languageSelect.value = 'All';
  language = 'All';
  languageSelect.remove();
}

//Рендер таблицы
tbodyRender(externalService.getAllCountries());

table.appendChild(tbody);
appRoot.appendChild(table);
